#!/usr/bin/env -S deno run --allow-write --allow-env --allow-run --allow-read

import {
  FilePath,
  assert,
  userAssert,
  mustParseJsonFromPath,
} from './common.ts';

import {
  PostsListing,
  OriginalPost,
  mustParsePostsFromPath,
} from './posts.ts';

import { basename } from "https://deno.land/std@0.201.0/path/posix.ts";

const { copyFile, errors, mkdir, writeTextFile } = Deno;

const postsJsonFile: PostsListing = Deno.args[0];
const srcDir = Deno.args[1];
const outDir = Deno.args[2];

userAssert(
  Deno.args.length === 3,
  "require one arg: POSTS_JSON SRC_DIR OUT_DIR");

const postsJson: PostsListing = await mustParsePostsFromPath(postsJsonFile);

function fileName(parentPath: string, postTitle: string): string {
  return `${parentPath}/index.adoc`;
}

// TODO move to cmmon lib
const INSTAGRAM_USERMENTION_REGEXP = /@(\w+(\w|-|_)*)/ig;
function markdownLinkInstagramUser(userName: string): string {
  return `https://instagram.com/${userName}[@${userName}, window=_blank]`
}

const INSTAGRAM_HASHTAG_REGEXP = /\#(\w+(\w|-|_)*)/ig;
function markdownHashtag(hashTag: string): string {
  const tick = '`';
  return `${tick}#${hashTag}${tick}`;
}

function instagramProseToMarkdown(prose: string): string {
  return prose.

      replace(
        INSTAGRAM_USERMENTION_REGEXP,
        (match, userName) => markdownLinkInstagramUser(userName)).

      replace(
        INSTAGRAM_HASHTAG_REGEXP,
        (match, hashTag) => markdownHashtag(hashTag));
}

const FRONT_MATTER_HEADER = '+++';
function postContent(post: OriginalPost, medias: Array<DestinationMedia>): string {
  let isoPublishDate = post.created.toISOString().slice(0, 10);

  let content = `${FRONT_MATTER_HEADER}
title = "${post.title}"
type = "post"
date = "${isoPublishDate}"
publishdate = "${isoPublishDate}"
${FRONT_MATTER_HEADER}

${instagramProseToMarkdown(post.prose)}

`;
  const mediaAsciidocLines = medias.map(m => `image::${m.baseName}[]`);
  content += mediaAsciidocLines.join('\n');

  return content + '\n';
}

// TODO move to common lib
async function ensureDirExists(dirPath: string) {
  try {
    await mkdir(dirPath);
  } catch (e) {
    if (e instanceof errors.AlreadyExists) return;
    throw e;
  }
}

interface DestinationMedia {
  dst: string,
  baseName: string,
}
async function migrateMediaForPost(srcParentPath: string, dstParentPath: string, srcMedias: Array<FilePath>): Array<DestinationMedia> {
  const medias: Array<{src: string, dst: string, baseName: string}> = srcMedias.map(src => {
    const mediaBaseName = basename(src);
    const dst = `${dstParentPath}/${mediaBaseName}`;
    return {src, dst, baseName: mediaBaseName};
  });
  for (let media of medias) {
    await copyFile(`${srcParentPath}/${media.src}`, media.dst);
  }
  return medias.map(m => {
    return {dst: m.dst, baseName: m.baseName};
  });
}

function postTitleToBasename(title: string): string {
  return title.
      // only single underscores
      replace(/_+/g, '_').

      // emdashes in prose become dashes with no padding
      replace(/\s-\s/g, '_').

      // spaces become dashes
      replace(/\s+/g, '-').

      // non alphanumerics get dropped
      replace(/[^\w|-]/ig, '');
}

console.error(`JSON parsed; writing all %s posts...`, postsJson.posts.length);
for (let post of postsJson.posts) {
  console.error(`\twriting post "${post.title}"`);

  const postBaseName = postTitleToBasename(post.title);

  // Make the destination for this markdown
  const dstDir = `${outDir}/${postBaseName}`;
  await ensureDirExists(dstDir);

  // Copy the static media
  const medias = await migrateMediaForPost(srcDir, dstDir, post.media);

  // Write the markdown
  await writeTextFile(
    fileName(dstDir, post.title),
    postContent(post, medias));
}
