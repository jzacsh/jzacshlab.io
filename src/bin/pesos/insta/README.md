# ports instagram posts to tiny markdown blog posts

This is a tiny quick-and-hacky script to port instagram `_posts.json` to tiny
markdown blog posts.

## step 1: generate useful json

```sh
$ unpack "${USER}_json-mode.zip" . # assume output is some/path/insta-dump/
$ ./munge-insta-json.ts  /some/path/insta-dump/_posts.json | tee | json > posts.json
```

## step 2: add a "title" field to each json object in the "posts" arary

modify `posts.json` (assuming you ran the same command above).

## step 3: generate markdown for all posts

Note that `some/src/dir` should be whatever the root path is of the inflated
instagram data-dump.

```sh
./posts-to-markdown.ts posts.json some/src/dir some/output/dir/
```
