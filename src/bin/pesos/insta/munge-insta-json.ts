#!/usr/bin/env -S deno run --allow-env --allow-run --allow-read

import {
  FilePath,
  assert,
  userAssert,
  mustParseJsonFromPath,
} from './common.ts';

import {
  InstagramTimestamp,
  instagramTimestampToDate,
  InstagramPostMedia,
  InstagramPostMultiMedia,
} from './instagram.ts';

import {
  OriginalPost,
  PostsListing,
} from './posts.ts';

const inputJson = Deno.args[0];

userAssert(
  Deno.args.length === 1,
  "require one arg: INPUT_JSON");

function instagramMediaToFilepath(media: InstagramPostMedia): FilePath {
  return media.uri;
}

function convertFromInstaSingle(single: InstagramPostMedia, sourceIndex: number,): OriginalPost {
  return {
    prose: single.title,
    created: instagramTimestampToDate(single.creation_timestamp),
    media: [instagramMediaToFilepath(single)],
  };
}

function convertFromInstaMulti(multi: InstagramPostMultiMedia): OriginalPost {
  return {
    prose: multi.title,
    created: instagramTimestampToDate(multi.creation_timestamp),
    media: multi.media.map(instagramMediaToFilepath),
  };
}

function instagramJsonToOriginalPosts(posts: InstagramPosts): Array<OriginalPost> {
  return posts.map((mediaOrMulti: InstagramPostMedia|InstagramPostMultiMedia, index: number): OriginalPost => {
    if (mediaOrMulti.title) {
      return convertFromInstaMulti(mediaOrMulti);
    } else {
      assert(
          mediaOrMulti.media.length === 1,
          `instagram exporter bug? single-media post (at index: ${index}) has multiple media somehow:\n"""\n${JSON.stringify(mediaOrMulti.media)}\n"""\n`);
      return convertFromInstaSingle(mediaOrMulti.media[0], index);
    }
  });
}

async function mustGetJson(jsonFilePath:  FilePath): Array<OriginalPost> {
  const instagramJson: InstagramPosts = await mustParseJsonFromPath(jsonFilePath);
  return instagramJsonToOriginalPosts(instagramJson);
}

const instagramPosts: PostsListing = {
  posts: await mustGetJson(inputJson),
}
console.log(JSON.stringify(instagramPosts));
