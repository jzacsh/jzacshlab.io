import {
  CommandlineError,
  loadLinesForCommand,
} from './shell.ts';

import {
  AssertionError,
  assert,
} from './ts/assert.ts';

import {
  fsFileExt,
  FsFilePath,
} from './ts/fs.ts';

import {optIsPresent} from './ts/optional.ts';

export interface Geometry2D {
  width: number;
  height: number;
}

export class NotAnImageError extends Error {
  filepath: string;
  constructor(filepath: string, message: string) {
    super(message, {cause: `not a recognized image: ${filepath}`});
    this.filepath = filepath;
  }

  static fromImageMagicErr(filepath: string): NotAnImageError {
    return new NotAnImageError(filepath, `imagemagick error`);
  }
}

export function isExpectedImageSuffix(filepath: string, expectedImageExts: Array<string>): boolean {
  const actualFileExt = fsFileExt(filepath);
  return optIsPresent(actualFileExt) &&
    expectedImageExts.includes(actualFileExt);
}

/** Whether filepath is an image of `expectedWidth` pixels wide. */
export async function isImageWidth(filepath: FsFilePath, expectedWidth: number): Promise<boolean> {
  const actualGeo = await mustGetImageSize(filepath);
  return actualGeo.width === expectedWidth;
}

/**
 * Thin wrapper for {@link getImageSize} that we're 100% certain sould be
 * possible to derive size of.
 */
export async function mustGetImageSize(filepath: string): Promise<Geometry2D> {
	const geo = await getImageSize(filepath);
	if (geo instanceof NotAnImageError) {
		throw new AssertionError(`bug? mustGetImageSize failed for "${filepath}", ${geo}`);
	} else {
		return geo;
	}
}

// Note: an alternative for width and height information for the SVG files is to
// use the width= and height= attribute that inkscape embeds into the svg root
// node. this can be read with an xpath expression passed to xmllint, like so:
//
//   ```bash
//   $ heighNum="$(xmllint --xpath 'string(//@height)' my.svg)"
//   $ widthNum="$(xmllint --xpath 'string(//@width)' my.svg)"
//   ```
export async function getImageSize(
  filepath: string,
  expectedImageExts: Array<string> = [],
): Promise<Geometry2D|NotAnImageError> {
  /**
   * See identify(1) utility from imagemagick. eg:
   *   ```sh
   *   $ identify -format '%[h]'
   *   $ identify -format '%[w]'
   *   ```
   */
  let cliResult: Array<string>;
  try {
    cliResult = await loadLinesForCommand([
      'identify',
      '-format',
      '%[w]\n%[h]',
      filepath,
    ]);
  } catch (cliError) {
    if (cliError instanceof CommandlineError &&
        !isExpectedImageSuffix(filepath, expectedImageExts)) {
      return NotAnImageError.fromImageMagicErr(filepath);
    }
    throw cliError;
  }

  assert(
      cliResult.length === 2,
      `expected width and height line for ${filepath} but got ${cliResult.length} lines from imagemagick (valid types: [${expectedImageExts.join(',')}]):\n"""\n${cliResult.join('\n')}\n"""\n`);

  function parsePixels(pixelStr: string, context: string): number {
    const result = parseInt(pixelStr, 10);
    assert(!isNaN(result), `failed parsing imagemagick's ${context} on ${filepath}`);
    return result;
  }
  return {
    width: parsePixels(cliResult[0], 'width'),
    height: parsePixels(cliResult[1], 'height'),
  };
}
