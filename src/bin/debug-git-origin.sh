#!/usr/bin/env bash

function usage() {
  printf 'usage: [-n: for name-only parsing] FILE_PATH\nERROR: %s\n' "$1"
  exit 1
}
(( $# <= 2 )) || usage 'too few args'
file="$1"
ParseGitDiffNameOnly=0
if [[ "$file" = -n ]]; then
  ParseGitDiffNameOnly=1
  file="$2"
  declare -a remainingArgs=(
    --name-only
  )
else
  declare -a remainingArgs=(
    --patch
  )
fi

[[ -e "$file" ]] || usage 'FILE_PATH missing'

function makeLogReadable() {
  if (( ParseGitDiffNameOnly )); then
    awk 'NR==1; END{print}'
  else
    head -n 8
  fi
}

git log \
  "${remainingArgs[@]}" \
  --format='%H' \
  --follow \
  --diff-filter=A \
  --find-renames=0 \
  "$file" |
  cat -n |
  makeLogReadable
printf 'WARNING! WARNING! this is not trustworthy for debugging until you compare it to the exact commandline embedded into the real script; these could trivially become out of sync!\n' >&2
