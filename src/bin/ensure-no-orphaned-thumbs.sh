#!/usr/bin/env bash
# shellcheck disable=SC2059

declare -r staticDir="$1"
declare -r thumbDir="$2"
function isReadableDir() { [[ -d "$1" && -r "$1" ]]; }
function logf() { printf -- "$@" >&2; } # stdout reserved for orphans printed
function dief() { printf -- "$@" >&2; exit 1; }
isReadableDir "$staticDir" ||
	dief 'bug! staticDir must be a readable DIR: "%s"\n' "$staticDir"
isReadableDir "$thumbDir" ||
	dief 'bug! thumbDir must be a readable DIR: "%s"\n' "$thumbDir"

# eg: thumbDir=static/img/_thumb we get:
# img/artwork/sketchbook/study_of_tie_in_starbucks_1.jpg
# img/artwork/sketchbook/study_of_tie_in_starbucks_2.jpg
# img/artwork/sketchbook/study_of_tie_in_starbucks_3.jpg
function listFilesRelativeTo() { find "$thumbDir" -type f -printf '%P\n' | sort; }

logf 'INFO: hunting for thumbnail orphans in %s at %s\n' \
  "$staticDir" \
  "$thumbDir"
oprhansFound=0
while read -r extantThumbSubPath; do
  [[ ! -e "$staticDir"/"$extantThumbSubPath" ]] || continue
	# guess at SVGs who have been converted to something with a 3 letter extension
  [[ ! -e "$staticDir"/"${extantThumbSubPath::-4}.svg" ]] || continue
	# guess at SVGs who have been converted to something with a 4 letter extension
  [[ ! -e "$staticDir"/"${extantThumbSubPath::-5}.svg" ]] || continue

	oprhansFound=$(( oprhansFound + 1 ))
  printf '%s/%s\n' "$thumbDir" "$extantThumbSubPath"
done < <(listFilesRelativeTo "$thumbDir")

(( oprhansFound == 0 )) ||
	dief 'ERROR: found %s orphans; see above output for which files to consider deleting\n' \
	"$oprhansFound" >&2
