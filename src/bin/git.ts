import {assert} from './ts/assert.ts';
import {
	assertOpt,
	optEmpty,
	Opt,
} from './ts/optional.ts';

import {FsFilePath} from './ts/fs.ts';
import {loadLinesForCommand} from './shell.ts';

const OPTS: {[Opt: string]: boolean|number} = {
  ParseGitDiffNameOnly: true,
  isDebugging: false,
};
export {OPTS};

export interface GitOrigin {
  originalFilepath: string;
  originatingCommitId: string;
}

export async function gitFindOrigin(filepath: string): Promise<GitOrigin> {
  /**
   * ```
   *     $ git log --name-only --format='%H' --follow --diff-filter=A --find-renames=0 -- moved-and-modified
   *  1  d87ff93c23dfe68f8ab1c3af7ff980
   *  2
   *  3  foo/bar/original/filepath.txt
   * ```
   */
  const remainingArg: string = OPTS.ParseGitDiffNameOnly ? '--name-only' : '--patch';
  const stdout: Array<string> = await loadLinesForCommand([
    'git',
    'log',
    '--format=%H',
    '--follow',
    '--diff-filter=A',
    '--find-renames=0',
    remainingArg,
    '--',
    filepath,
  ]);

  assert(
      stdout.length > 2,
      `got ${stdout.length} lines of output from git but expected more, for file "${filepath}"`);

  const commitHash: string = String(stdout[0] || '').trim();
  // dbg(`commitHash='${commitHash}'`);
  assert(commitHash.length, `first line should have had a commit hash, but got no output`);

  // TODO figure out how to implement this without this hacky scraping; eg: use
  // --format value or something?
  const scrapedFilePath = scrapeOriginalFilename(filepath, stdout);
  assert(scrapedFilePath.length, `found empty _to_filename for file: "${filepath}"`);

  return {
    originalFilepath: scrapedFilePath,
    originatingCommitId: commitHash,
  }
}

/**
 * Scrapes very particular `git log` output to try to determine the original
 * name of a file.
 *
 * ## Expected `git log` output
 *
 * `gitStdout` can have two forms: on for text fil contents that git is willing
 * to print to stdout, and another for "binary" files.
 *
 * The former output would look like this:
 *        ```
 *        $ git log --patch --format='%H' --follow --diff-filter=A --find-renames=0 -- moved-and-modified
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  diff --git a/2023-01-28T15:29:28-06:00 b/2023-01-28T15:29:28-06:00
 *     4  new file mode 100644
 *     5  index 0000000..b5dac5f
 *     6  --- /dev/null
 *     7  +++ b/2023-01-28T15:29:28-06:00
 *     8  @@ -0,0 +1 @@
 *     9  +Foo Bar Baz
 *        ```
 *
 * The latter output would look like this:
 *        ```
 *        $ git log --patch --format='%H' --follow --diff-filter=A --find-renames=0 -- file-never-moved
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  diff --git a/file-never-moved b/file-never-moved
 *     4  new file mode 100644
 *     5  index 0000000..58e8cca
 *     6  Binary files /dev/null and b/file-never-moved differ
 *        ```
 *
 * This is all true UNLESS OPTS.ParseGitDiffNameOnly is enabled in which cas a
 * different command is used with _seemingly_ more reliable output:
 *        ```
 *        $ git log --name-only --format='%H' --follow --diff-filter=A --find-renames=0 -- original/file/path.txt
 *     1  d8233d4fe968f58ab1c3af7ff9807ff93c
 *     2
 *     3  original/file/path.txt
 *        ```
 */
function scrapeOriginalFilename(currentFilePath: string, gitStdout: Array<string>): string {
  function errMsg(baseMsg: string): string {
    const addition = !OPTS.isDebugging ?
        '' :
        `[extra -debug output]: OPTS.ParseGitDiffNameOnly=${OPTS.ParseGitDiffNameOnly}; git stdout lines were:\n"""\n${gitStdout.join('\n')}\n"""\n`;
    return `fragile scraper failure: ${
        baseMsg} for file: "${
        currentFilePath}"\n\tconsider trying bin/debug-git-origin.sh on this file\n${addition}`;
  };
  const bSlashRegExp = /b\//;
  if (OPTS.ParseGitDiffNameOnly) {
    assert(
        gitStdout.length === 3,
        errMsg(`expected exactly 3 lines`));
    const lastLine: string = String(gitStdout[2] || '').trim();
    assert(lastLine.length, errMsg(`expected non-empty last-line`));
    return lastLine;
  }

  const origFileLineNum: number = gitStdout.findIndex(line => line === '--- /dev/null');
  if (origFileLineNum === -1) { // this was NOT a file rename
    assert(
        gitStdout.length >= 6,
        errMsg(`short on diff line-count (${gitStdout.length}) for "binary" file diff`));
    const lastLine: string = String(gitStdout[gitStdout.length-1] || '').trim();
    assert(
        lastLine.length,
        errMsg(`unexpected empty final line of "binary diff" output`));

    assert(
        lastLine.startsWith('Binary files ') && lastLine.endsWith(' differ'),
        errMsg(`unrecognized overall content (expected "binary diff" output)`));

    const scrapedOriginalFilePath: string = lastLine.
        replace(/^Binary files .* and /, '').
        replace(/ differ$/, '').
        replace(bSlashRegExp, '').
        trim();
    // TODO consider running `git show COMMITID scrapedOriginalFilePath` as a
    // sanity-check that we scraped a filepath and not some other random strings
    // out of the diff output.
    assert(
        scrapedOriginalFilePath.length,
        errMsg(`turned up empty results`));
    return scrapedOriginalFilePath;
  } else {
    assert(
        gitStdout.length > 7,
        errMsg(`short on diff line-count (${gitStdout.length}) for "text" diff`));

    assert(
        origFileLineNum < gitStdout.length - 2,
        errMsg(`found /dev/null on last line of output but expected _to_ filename`));
    /**
     * from:
     *   """
     *   +++ b/some/file/path.txt
     *   """
     * to:
     *   """
     *   some/file/path.txt
     *   """
     */
    const scrapedFilePath: string =
        String(gitStdout[origFileLineNum+1] || '').
        replace(/^\+.. /, '').
        replace(bSlashRegExp, '').
        trim();
    assert(
        scrapedFilePath.length,
        errMsg(`expected non-empty _to_ filename line`));
    return scrapedFilePath;
  }
}

/**
 * Whether `dependentFile` was last modified from a point in history that is
 * actually older than `originFile`'s current history.
 *
 * Useful when `originFile` is somehow a dependency of `dependentFile`, both are
 * tracked by git, and we want to know if we can prove (with git history alone)
 * that dependentFile _must_ be outdated. Assumes `dependentFile` updates are
 * only made in a clean tree (that is: `dependentFile` and `originFile` are not
 * updated in the same commit).
 *
 * ## Example Usage
 *
 * For example, suppose dependency `a.out` is a compiled binary you're tracking
 * with git (for some reason) and `main.c` is the only build input (also tracked
 * by git), then originFile="main.c" and dependentFile="a.out". This algorithm
 * assumes `a.out` is only re-compiled if `main.c` updates are fully committed.
 *
 * Here's an illustration of this same example where HEAD is rev 'c':
 *
 * ```
 *  c * Bugfix: wasn't beautiful enough yet. 
 *  |   main.c
 *  |  
 *  b * First release of a.out, yay!
 *  |   a.out
 *  |  
 *  a * My beautiful main program!
 *      main.c
 * ```
 *
 * In this case `gitIsDependentFresh('main.c', 'a.out')` should return false
 * because a.out was last built from its dependent 'main.c' at rev 'a', but
 * 'main.c' has since been updated in rev 'c', so we can assume 'a.out' is no
 * longer fresh and now requires a recompile (a commit 'd' is needed).
 *
 * NOTE: dependentFile need not yet have commit history, in the case this is the
 * first run of this function (say HEAD is at commit 'a' in the above example
 * usage).
 */
export async function gitIsDependentFresh(originFile: FsFilePath, dependentFile: FsFilePath): Promise<boolean> {
  const origCurrHash = assert(
    await gitHash(originFile),
    `usage error: checking hash of an file that git does not track: "${originFile}"`,
  );

  // commit of last depdendent's modificatoin
  const dependentBuiltAt: Opt<GitRev> = await gitRevLastTouch(dependentFile);
  if (dependentBuiltAt === undefined) {
    return false; // dependent doesn't even exist yet, so trivially "not fresh"
  }

  // parent commit: ie: what commit dependent built atop of?
  const dependentBuiltFrom: GitRev = assertOpt(
		dependentBuiltAt,
		'bug? already checked undefined',
	) + '^';

  const origLastBuiltHash = assert(
    await gitHash(originFile, dependentBuiltFrom),
    `dependency "${
        originFile}" does not go back before its supposed dependent "${
        dependentFile}": ${
        dependentBuiltFrom}`,
  );

  return origLastBuiltHash === origCurrHash;
}

export type GitHash = string;
export type GitRev = string|GitHash;

/**
 * Return hash git has recorded for filePath at rev in its history or undefined
 * if `filePath` is not recorded in git history.
 */
export async function gitHash(filePath: string, rev: GitRev = 'HEAD'): Promise<Opt<GitHash>> {
  const revPath = String.raw`${rev}:${filePath}`;
  const stdout = await loadLinesForCommand([
    'git',
    'rev-parse',
    revPath,
  ]);
  const lines = stdout && stdout.length ? stdout : [];
  assert(
    lines.length <= 1,
    `bug? git rev-parse returned more than one line (${lines.length}) for "${revPath}"`);
  return lines.length === 1 ? lines[0] : optEmpty<GitHash>();
}

/**
 * Return rev git has recorded as the last time filePath was modified,
 * or undefined if filePath is not recorded in git history.
 */
export async function gitRevLastTouch(filePath: string): Promise<Opt<GitRev>> {
  const revs = await getRevsTouching(filePath, 1 /*opt_limit*/);
  return revs && revs.length ? revs[0] : optEmpty<GitHash>();
}

/**
 * Return revs that touched `filePath` or an empty array if none were found.
 * Values are listed in reverse chronological order (most recent revs listed
 * first).
 *
 * Optionally limit to only the most recent `opt_limit` number of revs.
 */
async function getRevsTouching(filePath: string, opt_limit?: number): Promise<Array<GitRev>> {
  const cliLeadingArgs = [
    'git',
    'log',
  ];
  if (opt_limit !== undefined) {
    cliLeadingArgs.push(`-${opt_limit}`);
    cliLeadingArgs.push('HEAD');
  }
  const stdout = await loadLinesForCommand([
    ...cliLeadingArgs,
    '--pretty=%H',
    '--follow',
    '--',
    filePath,
  ]);

  if (opt_limit !== undefined) {
    assert(
        stdout.length <= opt_limit,
        () => `bug: 'git log' was limited to ${
          opt_limit} history length, but returned ${
          stdout.length} logs:\n\t${
          stdout.join('\n')}`,
    );
  }

  return stdout && stdout.length ? stdout : [];
}
