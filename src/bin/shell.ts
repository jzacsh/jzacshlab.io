import {
  assert,
} from './ts/assert.ts';
import {Opt} from './ts/optional.ts';

export type CommandLine = Array<string>;

/**
 * Maps known[1] Deno errors, as the need arrises, to a context-specific message
 * for this package's exported functionality.
 *
 * [1]: https://deno.land/api@latest?s=Deno.errors
 */
function explainDenoError(e: unknown): string {
  if (e instanceof Deno.errors.NotFound) {
    return `is package in $PATH or program installed on system?: ${e}`;
  }
  return 'unknown cause: ${e}';
}

export class CommandlineError extends Error {
  stderr?: Uint8Array;

  private constructor(
    message: string,
    stderr?: Uint8Array) {
    super(message);
    this.stderr = stderr;
  }

  private static messageFromCmd(
    commandline: CommandLine,
    moreInfo?: string,
  ): string {
    const cmd: string = commandline[0];

    let msg = `command \`${cmd}\` failed; full commandline was: "${
       commandline.join(' ')}"`;
    if (moreInfo) {
      msg += `; ${moreInfo}`;
    }
    return msg;
  }

  static fromThrownError(
    commandline: CommandLine,
    error: unknown,
  ) {
    const message: string = CommandlineError.messageFromCmd(
        commandline, explainDenoError(error));
    return new CommandlineError(message);
  }

  static assertCli(
    assertion: unknown,
    commandline: CommandLine,
    stderrRaw?: Uint8Array,
  ) {
    if (assertion) return;
    throw CommandlineError.fromCommandline(commandline, stderrRaw);
  }

  static fromCommandline(
    commandline: CommandLine,
    stderrRaw?: Uint8Array,
  ) {
    let extraInfo: Opt<string> = undefined;
    if (stderrRaw) {
      const stderr: string = new TextDecoder().decode(stderrRaw);
      extraInfo = `got stderr:\n"""\n${stderr}\n"""`;
    }

    const message: string = CommandlineError.messageFromCmd(
        commandline, extraInfo);
    return new CommandlineError(message);
  }
}

export async function loadLinesForCommand(commandline: CommandLine): Promise<Array<string>> {
  assert(commandline.length, `non-empty commandline required`);
  const callable: string = assert(
    commandline.shift(),
    `got CLI with empty first word; remaining args: [${commandline.join(', ')}]`,
  );

  let command: Deno.Command;
  try {
    command = new Deno.Command(callable, {
      args: commandline,
      stdout: 'piped',
      stderr: 'piped',
    });
  } catch (denoRunError: unknown) {
    throw CommandlineError.fromThrownError(commandline, denoRunError);
  }

  const { code, stdout, stderr } = await command.output();
  CommandlineError.assertCli(code === 0, commandline, stderr);

  const lines: Array<string> = new TextDecoder().
      decode(stdout).
      trim().
      split('\n');
  if (!lines.length) return [];
  if (lines.length === 1 && !lines[0].trim()) return [];
  return lines;
}
