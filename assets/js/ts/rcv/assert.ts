import { toPlainOldObject } from './debug.ts';

type AssertMsgFn = () => string;
export type AssertMsg = string | AssertMsgFn;

function assertMsg(msg: AssertMsg): Error {
  return typeof msg === 'function' ? msg() : msg;
}

export type UnAssertAny = UnAssertExtant | 0;
export type AssertedAny<T> = T & Exclude<keyof T, UnAssertAny>;
export function assertAny<T>(subject: T | UnAssertAny, msg: AssertMsg): AssertedAny<T> {
  if (!subject) throw new Error(`assert(truthy): ${assertMsg(msg)}`);
  return subject as AssertedAny<T>; // dirty but works
}

export function assert<T>(
  subject: T | UnAssertAny,
  msg: string,
): AssertedAny<T> {
  return assertAny(subject, msg);
}

export type UnAssertExtant = UnAssertDef | null;
export type AssertedExtant<T> = T & Exclude<keyof T, UnAssertExtant>;
/** Asserts `subject appears to exist. */
export function assertExtant<T>(subject: T | UnAssertExtant, msg: AssertMsg): AssertedExtant<T> {
  if (subject === null || subject === undefined) throw new Error(`assert(exists): ${assertMsg(msg)}`);
  return subject as AssertedExtant<T>; // dirty but works
}

export type UnAssertDef = undefined;
export type AssertedDef<T> = T & Exclude<keyof T, UnAssertDef>;
/** Asserts `subject is defined. */
export function assertDef<T>(subject: T | UnAssertDef, msg: AssertMsg): AssertedDef<T> {
  if (subject === undefined) throw new Error(`assert(def): ${assertMsg(msg)}`);
  return subject as AssertedDef<T>; // dirty but works
}


export function countDupes(arr: Array<unknown>): number {
  const qtyUniq = new Set(arr).size;
  return arr.length === qtyUniq
      ? 0
      : arr.length - qtyUniq;
}

/** Array where all T are unique. */
export type UniqArray<T> = Array<T>;

export function assertGet<K, V>(
  key: K,
  container: Map<K, V>,
  context: AssertMsg): V  {
  return assertDef(
      container.get(key),
      () => `assertGet(key="${key}"): ${assertMsg(msg)}`);
}


export function getAnyKeyWithVal(
  plainObject: Object,
  needle: unknown,
  context: AssertMsg,
): string {
  for (let key in plainObject) {
    if (plainObject[key] === needle) {
      return key;
    }
  }
  throw new Error(
    `getAnyKeyWithVal(${
      toPlainOldObject(needle)
    }): no needle found: ${
      assertMsg(context)}: ${
      JSON.stringify(toPlainOldObject(plainObject))}`);
}

export function assertPositiveInt(
    val?: string|number,
    assertContext: string,
): number {
  function assertMsg(microContext: string): string {
    return `assert(positive int): ${assertContext}: ${microContext} "${val}"`;
  }
  assert(
      val !== undefined && val !== null &&
      (typeof val === 'string' || typeof val === 'number'),
      () => assertMsg('not a positive integer'));
  assert(
      typeof val !== 'string' ||
          val.match(/^\d+$/),
      () => assertMsg('non-integer characters'));
  const parsed = parseInt(val, 10);
  assert(
      !isNaN(parsed) && parsed >= 0,
      () => assertMsg('not parsing as a non-negative integer'));
  return parsed;
}
