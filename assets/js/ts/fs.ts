import {assert} from './assert.ts';
import {strTrim, strLastChar} from './string.ts';
import {
  optIsEmpty,
  optEmpty,
  Opt,
} from './optional.ts';

type FsDelimiter = '/';
export const FsDelim: FsDelimiter = '/';

/** Filepath can be relative or absolute, but cannot be empty string. */
export type FsFilePath = string;
type FsFilePathPart = Exclude<FsFilePath, FsDelimiter>;

function fsDelimTrim(path: FsFilePath): string {
  return strTrim(path, FsDelim);
}

export function fsPathJoin(...pathPart: FsFilePathPart[]): FsFilePath {
  const parts = Array.of(...pathPart);
  assert(parts.length >= 2, `only ${parts.length} path parts; nothing to join`);

  const cleanedParts: Array<string> = parts.
      map(part => fsDelimTrim(part)).
      filter(part => part);
  assert(parts.length >= 2, `only ${parts.length} path parts; nothing to join`);

  const leadingDelim: boolean = parts[0][0] === FsDelim;
  if (leadingDelim) cleanedParts.splice(0 /*start*/, 0 /*deleteCount*/, '');

  const trailingDelim: boolean = strLastChar(parts[parts.length - 1]) === FsDelim;
  if (trailingDelim) cleanedParts.push('');

  return cleanedParts.join(FsDelim);
}

function getFileExtMatcher(filepath: FsFilePath): Opt<RegExp> {
  const ext = fsFileExt(filepath);
  if (optIsEmpty(ext)) return optEmpty();
  return new RegExp(`\.${ext}$`);
}

export function fsFileExt(filepath: FsFilePath): Opt<FsFilePathPart> {
  const fileSuffixMatch =
      assert(
          baseName(filepath),
          `could not determine basename of "${filepath}"`).
      match(/\.(\w+)$/);
  if (!fileSuffixMatch) return '';
  return fileSuffixMatch[1].toLowerCase().trim() || undefined;
}

export function removeExtension(path: FsFilePath): FsFilePath {
  const extMatcher = getFileExtMatcher(path);
  if (!extMatcher) return path;
  return path.replace(extMatcher, '');
}

export function replaceExtension(
  path: FsFilePath,
  toExt: FsFilePathPart,
): string {
  const replaceRegexp: RegExp = assert(
      getFileExtMatcher(path),
      `can only extension on file with one present (got file "${
        path}")`);
  return path.replace(replaceRegexp, `.${toExt}`);
}

export function dirName(path: FsFilePath): FsFilePath {
  return pathParts(path).dirname || '';
}

export function baseName(path: FsFilePath): FsFilePathPart {
  return pathParts(path).basename;
}

export interface PathParts {
  // eg: "foo" (from a broader "upper/foo") or "foo/bar" (from a broader "foo/bar/baz").
  dirname?: FsFilePath;
  // eg: "baz" (from a broader "foo/bar/baz", or just from "baz" or "/baz")
  basename: FsFilePathPart;
}

export function pathParts(pathRaw: FsFilePathPart): PathParts {
  const path: string = String(pathRaw).trim();
  assert(path.length, `path must be non-empty; got: "${pathRaw}"`);

  const parts: Array<string> = path.split(FsDelim);
  if (parts.length === 1) {
    return {basename: path};
  }

  const lastIndex = parts.length - 1;

  return {
    dirname: parts.slice(0, lastIndex).join(FsDelim),
    basename: parts[lastIndex],
  }
}

/** Given a likely detox(1)-friendly filename, try to guess at a likely "title". */
export function pseudoTitleFromFileName(fileName: FsFilePath): string {
  return removeExtension(baseName(fileName)).
    replace(/-/g, ' ').
    replace(/_/g, ' ');
}
