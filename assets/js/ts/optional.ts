import {
  AssertDebugger,
  assert,
  Asserted,
  Unasserted,
  AssertFalsey,
} from './assert.ts';

export type Opt<T> = Unasserted<T>;

export function optIsEmpty<T>(opt: Opt<T>): opt is AssertFalsey {
  return opt === undefined;
}

export function optIsPresent<T>(opt: Opt<T>): opt is Asserted<T> {
  return !optIsEmpty(opt);
}

export function optEmpty<T>(): Opt<T> {
  return undefined;
}

export function assertOpt<T>(opt: Opt<T>, asrtDbg: AssertDebugger): Asserted<T> {
  return assert(opt, asrtDbg);
}
