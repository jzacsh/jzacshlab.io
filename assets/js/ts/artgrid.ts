import {assert} from './assert.ts';
import {inPlaceShuffle} from './common.ts';
import {Dom, ElemAttrs} from './browser.ts';
import {TargetSmallWidthPx, StaticHost} from './art.ts';
import {ArtworkIndex, listArtWorks, Artwork, ArtworkDir} from './artapi.ts';
import {pseudoTitleFromFileName} from './fs.ts';

/**
 * @fileoverview This file conforms to the template expectations of
 * app/layouts/jslib.html; namely: this file expects jslibJsArgs to be a window
 * global containing the Google Drive HOST id of a public folder under which it
 * can find:
 *   1) an index.json file at the root listing the folder's contents
 *   2) art work to be displayed in this template.
 */

///////////////////////////////////////////////////////////////////////////////

export class ArtGrid {
  private static readonly LOADING_INDICATOR_ATTR='data-artgrid-state'

  /** Standard size of sections to add to grid. */
  private static readonly GRID_ADDITION: number = 8;

  private containerEl: Element;
  private gridEl: Element;
  private randomizedWorksToLoad: Array<Artwork>;
  private constructor(
    private artIndex: ArtworkIndex,
    private staticHost: StaticHost,
    private dom: Dom,
    rootEl: Element,
  ) {
    this.containerEl = ArtGrid.injectContainer(this.dom, rootEl);
    this.gridEl = this.dom.buildEl('ul');
    this.randomizedWorksToLoad = inPlaceShuffle(
      listArtWorks(this.artIndex).
          filter(art => this.isGoodAdd(art.filename)),
    );

    // DOM mutation starts here; Must be the last line of constructor
    this.buildGrid();
  }

  /** Builds artwork grid and attaches to DOM as soon as browser's ready. . */
  static init(w: Window): Promise<unknown> {
    const dom = new Dom(w.document);
    return dom.ready().
      then(() => {
        let rootNode: Element = dom.queryOne('section#matter');
        let host = new StaticHost();
        return host.
            fetchIndex().
            then((index: ArtworkIndex) => new ArtGrid(index, host, dom, rootNode));
      }).
      then(grid => grid.renderArtworks());
  }

  private buildGrid() {
    Dom.setAttrs(this.gridEl, {'class': 'grid'});
    this.containerEl.appendChild(this.gridEl);

    let buttonEl: Element = this.dom.buildEl('button');
    buttonEl.textContent = 'Load more';
    buttonEl.addEventListener('click', () => {
      const moreWorkPending: boolean = this.addRandomArt();
      if (moreWorkPending) return;
      buttonEl.remove();
    });
    this.containerEl.appendChild(buttonEl);
  }

  /**
   * @param {!Document} doc
   * @param {!Element} rootNode
   * @return {!Element} container
   * @private
   */
  private static injectContainer(dom: Dom, rootNode: Element) {
    return dom.appendNewEl(rootNode, 'div', {'id': 'artwork'});
  }

  private renderArtworks() {
    assert(this.addRandomArt(), 'already out of artwork');
    this.markLoaded();
  }

  private markLoaded() {
    const loadingIndicatorQuery = `[${ArtGrid.LOADING_INDICATOR_ATTR}]`;
    const loadingEl = this.dom.doc.querySelector(loadingIndicatorQuery);
    if (!loadingEl) {
      console.error(
        `failed to remove loading indicator: query failed for "${
          loadingIndicatorQuery}"`);
      return;
    }
    loadingEl.setAttribute(ArtGrid.LOADING_INDICATOR_ATTR, 'DONE');
  }

  /** Renders a visual grid of artwork on the page. */
  addToGrid(artwork: Array<Artwork>) {
    artwork.forEach((work: Artwork) => {
      if (!this.isGoodAdd(work.filename)) {
        return;
      }

      this.addArtToGrid(work);
    });
  }

  private isGoodAdd(artwork: string): boolean {
    // TODO(zacsh): deal with this when you break artwork into "vector" vs
    // "raster" vs "doodle"
    return Boolean(
        // TODO figure out why this was added and/or delete this condition
        typeof artwork != 'object' &&

        // TODO delete this (the index builder prevents this from showing up now
        artwork != 'copying');
  }

  /**
   * Appends a thumbnail for `fileName` to ArtGrid grid.
   *
   * @return newly appended thumbnail for {@code fileName}
   */
  addArtToGrid(artwork: Artwork): Element {
    let fileUrl: string = '/' + artwork.filename;
    const pseudoTitle = pseudoTitleFromFileName(artwork.filename);

    let thumbNailEl: Element = this.dom.appendNewEl(
        this.gridEl,
        'li',
        {class: 'thumbnail'});

    let imgAnchorEl: Element = this.dom.appendNewEl(thumbNailEl, 'a', {
      target: '_blank',
      href: fileUrl,
    });

    let imgEl: Element = this.dom.appendNewEl( imgAnchorEl, 'img', {
      src: this.staticHost.toThumbPath(fileUrl),
      alt: `sketch: ${pseudoTitle}`,
    });
    // TODO(zacsh) remove this hard-coded performance net for the browser, once
    // i am serving multiple sizes
    imgEl.setAttribute('width', TargetSmallWidthPx+'px');

    this.addCaptionArea(imgAnchorEl, artwork);

    return thumbNailEl;
  }

  private addCaptionArea(thumbnail: Element, artwork: Artwork) {
    const captionSpanEl = this.dom.appendNewEl(
        thumbnail, 'p', {class: 'caption'});

    const titleEl = this.dom.appendNewEl(
        captionSpanEl, 'span', {class: 'work-title'});
    titleEl.textContent = pseudoTitleFromFileName(artwork.filename);

    if (artwork.metadata) {
      const descriptionEl = this.dom.appendNewEl(
        captionSpanEl, 'span', {class: 'description'});
      descriptionEl.textContent += "Created: ";
      descriptionEl.textContent += artwork.metadata.created.join(',');
    }
  }

  /**
   * Loads random small subset of artwork into the grid.
   *
   * @return whether there's still more artwork waiting to be loaded.
   */
  addRandomArt(): boolean {
    const nextRandomWorks =
      this.randomizedWorksToLoad.splice(0, ArtGrid.GRID_ADDITION);
    this.addToGrid(nextRandomWorks);
    return !!this.randomizedWorksToLoad.length;
  }
}
