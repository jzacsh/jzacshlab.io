/** Sort result indicating `a` comes before `b`. */
export const SortsLess: SortResult = -1;

/** Sort result indicating `a` equals `b`. */
export const SortsEqual: SortResult = 0;

/** Sort result indicating `a` comes after `b`. */
export const SortsGreater: SortResult = 1;

/**
 * Common sort API result when comparing objects `a` and `b`.
 * - return -1 to indicate a comes before b.
 * - return 0 to indicate they are equal
 * - return 1 to indicate a comes after b.
 */
export type SortResult = -1 | 0 | 1;
