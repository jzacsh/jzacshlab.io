import {assert} from './assert.ts';

/** Sorts `a` in-place(!!) and returns it purely for ergonomics. */
// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
export function inPlaceShuffle<T>(a: Array<T>): Array<T> {
  for (let i = 0; i < a.length; i++) {
    let randomOtherIndex = Math.floor(Math.random() * (i + 1));

    let displacedItem = a[i];
    a[i] = a[randomOtherIndex];
    a[randomOtherIndex] = displacedItem;
  }
  return a;
}

export function keepNDecimals(num: number, decsToKeep: number): number {
  assert(decsToKeep >= 0 && decsToKeep < 10, 'programmer bug: asking for funny things');
  const shiftMagnitude = Math.pow(10, decsToKeep);
  const shiftedLeft = num * shiftMagnitude;
  return Math.floor(shiftedLeft) / shiftMagnitude;
}

function countDigits(val: number): number {
  const natural: number = Math.abs(val);
  if (val === 0) return 1;
  let digits = 1;
  while (val >= 10) {
    digits++;
    val = val % 10;
  }
  return digits;
}

function bitsToBaseStr(bits: ArrayBuffer, base: number): string {
  assert(base > 1, `nonsensical base given (got "${base}")`);
  const digits: number = countDigits(base);
  return Array.
      from(new Uint8Array(bits)).
      map(b => b.toString(base).padStart(digits, '0')).
      join('');
}

function bitsToHex(bits: ArrayBuffer): string {
  return bitsToBaseStr(bits, 16 /*base*/);
}

export type DigestHash = string;

export async function digestOf(inputRaw: string): Promise<DigestHash> {
  const input = new TextEncoder().encode(inputRaw);
  const digest = await crypto.subtle.digest('SHA-1', input);
  return bitsToHex(digest);
}
