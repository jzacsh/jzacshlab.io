import {digestOf, DigestHash} from './common.ts';
import {assert} from './assert.ts';
import {
  SortResult,
  SortsLess,
  SortsEqual,
  SortsGreater,
} from './sort.ts';

/* TODO(tsc wrangling) - how much of this type ceramony can i finally delete?*/
export type ArtworkMetadataVal = string;
export type ArtworkMetadataValues = Array<ArtworkMetadataVal>;

export type ArtworkMetadataType = keyof ArtworkMetadata;
// TODO can we use this, and then also have ArtworkMetadataStats utilize that
// somehow so both structs are nearly identicall defined?
// export type ArtworkMetadataType = 'color'|'content'|'created'|'media'|'type';
export function listValidArtworkMetadataTypes(): Array<ArtworkMetadataType> {
  return [
    'color',
    'content',
    'created',
    'media',
    'type',
  ];
}

/** Only call if you cannot have your parameter _itself_ have a type of {@link ArtworkMetadataType}. */
export function isValidArtworkMetadataKey(key: string): boolean {
  // we purposely cast because we want to force String#includes to compare our
  // string to what we know is only ArtworkMetadataType values.
  return listValidArtworkMetadataTypes().
      includes(key as ArtworkMetadataType);
}

/**
 * To find currently utilized values - of which if there are no typos, they
 * should be in this interface - run this command:
 *    $ cut -f 1 -d ' ' static/img/artwork/sketchbook/*.metadata  | sort | uniq
 */
export interface ArtworkMetadata {
  color: ArtworkMetadataValues;
  content: ArtworkMetadataValues;
  created: ArtworkMetadataValues;
  media: ArtworkMetadataValues;
  type: ArtworkMetadataValues;

  // TODO reconsider this
  [k: string]: ArtworkMetadataValues;
  // WARNING: adding something here should correspond to adding something in
  // isValidArtworkMetadataKey and ArtworkMetadataStats
  // TODO can remove all this wonkiness by using `keyof MyType` instead?
}

export interface ArtworkIndex {
  contents: ArtworkDir;
  metadataStats: ArtworkMetadataStats;
}

interface FsDirEntry {
  filename: string;
}

export function sortFsDirEntry(a: FsDirEntry, b: FsDirEntry): SortResult {
  if (a.filename === b.filename) return SortsEqual;
  return a.filename < b.filename ? SortsLess : SortsGreater;
}

// TODO this should be Array<Artwork|ArtworkDir>
// fix that^ and then delete this type (just inline it below to ArtworkDir
// definition). Can probably implify sortArtworkDirChild if that happens.
export type ArtworkDirChild = Artwork|ArtworkDir;

// see https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
export function isChildArt(child: ArtworkDirChild): child is Artwork {
  return (child as Artwork).artworkId !== undefined;
}

// see https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
export function isChildDir(child: ArtworkDirChild): child is ArtworkDir {
  return !isChildArt(child);
}

export function sortArtworkDirChild(a: ArtworkDirChild, b: ArtworkDirChild): SortResult {
  // TODO this  code ignores the types of a,b, becauase of the TODO on ArtworkDirChild being wrong. Fix.
  if (isChildArt(a) && isChildArt(b)) {
    return sortArtwork(a, b);
  } else if (isChildDir(a) && isChildDir(b)) {
    return sortFsDirEntry(a, b);
  } else if (isChildArt(a)) {
    return SortsLess;
  } else { // only b is artwork
    return SortsGreater;
  }
}

export interface ArtworkDir extends FsDirEntry {
  children: Array<ArtworkDirChild>;

  // Count of the direct child files (not dirs).
  immediateLength: number;

  // Count of all files (not dirs) recursively.
  recursiveLength: number;
}

export type ArtMetadataHash = DigestHash;
/** Should only be constructed via {@link buildDefaultValueFreq}. */
export interface ArtworkMetadataFreq {
  value: string;
  hash: ArtMetadataHash; // eg: md5sum of value
  freq: number; // float in (0,1]
  count: number; // natural number
}

export interface ArtworkMetadataStats {
  color: Array<ArtworkMetadataFreq>;
  content: Array<ArtworkMetadataFreq>;
  created: Array<ArtworkMetadataFreq>;
  media: Array<ArtworkMetadataFreq>;
  type: Array<ArtworkMetadataFreq>;

  // TODO reconsider this
  [k: string]: Array<ArtworkMetadataFreq>;
}

export interface Artwork extends FsDirEntry {
  artworkId: string;
  ratio: number;
  width: number;
  height: number;
  metadata?: ArtworkMetadata;
}

export function sortArtwork(a: Artwork, b: Artwork): SortResult {
  if (a.artworkId === b.artworkId) return SortsEqual;
  return a.artworkId < b.artworkId ? SortsLess : SortsGreater;
}

export async function buildDefaultValueFreq(value: string): Promise<ArtworkMetadataFreq> {
  const hash: ArtMetadataHash = await digestOf(value);
  return {
    value,
    hash,
    count: 0,
    freq: 0,
  };
}

export function listArtWorks(index: ArtworkIndex): Array<Artwork> {
  function listDirWorks(dirContents: Array<ArtworkDirChild>): Array<Artwork> {
    const immediateWorks: Array<Artwork> = dirContents.
        filter(c => isChildArt(c));
    const subDirWorks: Array<Artwork> = dirContents.
        filter(c => isChildDir(c)).
        flatMap(d => listDirWorks(d.children));
    return immediateWorks.concat(subDirWorks);
  }
  return listDirWorks(index.contents.children);
}
