+++
title = "Chat Directly"
type = "page"
+++

:hide-uri-scheme:

:mastodon: https://joinmastodon.org
:matrixTo: https://matrix.to/#/@jzacsh:matrix.org
:pgp: /pgp.asc

:e2ee: https://element.io/blog/senators-implore-department-of-defense-to-expand-the-use-of-matrix
:e2eeChat: footnote:e2eeChat[Has the benefit of being easy to chat \
   {e2ee}[privately], unlike WhatsApp, Hangouts, SMS, typical email, etc.]
:remnants: footnote:timeline[The last couple "timelines" I have are as \
   empty as my Facebook has always been, but if you're curious I'm on \
   {mastodon}[the Mastodon network] at https://qoto.org/@jzacsh (and a private \
   https://instagram.com/jzaksh)]
:keyserverLookup: https://keyserver.ubuntu.com/pks/lookup?fingerprint=on&op=index&search=0xa1a4d5ee0a7ddcc6be6f362d6da4194060d602c2
:nostrJumpMe: https://njump.me/npub170ar5c8dq7nk6k2w5vrr3qm9yrdc7e3nq9yxxd06tyt3ha9yqvgs76ttdn
:nostr: https://wikipedia.org/wiki/Nostr
:nostrDm: https://primal.net/e/note1p29va7zm9fr2hm0tc7e52zxjzm58sxymxdtqffleeklp5ce4n6fs0gpwnq
:nostrChatE2ee: footnote:nostrChatE2ee[nostr chats have a public trail that \
  we've chatted, but all chat content is actually e2ee.]

== Real Way to Reach Me

.Ways to reach me, in order of preference:
. If you have my phone number, chat via https://signal.org messenger{e2eeChat},
. Email j@zac.sh (optional PGP: link:/pgp.txt[`0x0D602C2` key asciiarmor] or {keyserverLookup}[its keyserver listing])
. Imperfect as these are IMO a lot less accessible today (2025-02), but
   hopefully easier as time goes on:
 .. via Matrix: {matrixTo}[`@jzacsh:matrix.org`], directly{e2eeChat}.
 .. (_extra experimental!_) via {nostr}[nostr] over {nostrDm}[direct
    message]{e2eeChat} to (eg: via primal's desktop web UI); I'm:
     {nostrJumpMe}[`npub170ar5c8dq7nk6k2w5vrr3qm9yrdc7e3nq9yxxd06tyt3ha9yqvgs76ttdn`]{nostrChatE2ee}.

== No but where's your Timeline?

If the above links appear useless because you simply want to know where to
"follow" me - I truly don't upkeep{remnants} a public profile anywhere (and I
haven't since I last used Facebook around 2006-2009).

But really just *email or chat me above* and say Hi, and  we'll stay in touch :)
