+++
title = "Ear close-up - made 20221228"
type = "post"
date = "2023-03-19"
publishdate = "2023-03-19"
+++

Studying anatomy and portraiture, I've had a lot of fun focusing on ears in some sketches. They're so complicated and fun to draw.

Drawn 2022-12-28 with pen and ink (a Delike pocket pen with Platinum Carbon Black, I think), about A5 sized paper. `#livemodel` `#lifedrawing` `#inkdrawing` `#fountainpen`

image::336485019_741535407617859_2898577828257603622_n_17968098173202261.jpg[]
