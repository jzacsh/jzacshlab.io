+++
title = "Sketchbook full o dogs - made 20230212"
type = "post"
date = "2023-03-28"
publishdate = "2023-03-28"
+++

So I've been drawing a LOT the last few months and though my goals are around
improving in human anatomy and portraiture, my doggos are just always around. Also
they're my favorite people. So whether I like it or not, 50% of my sketchbook is
filled with tiny little studies of their faces and their napping and paws.

The one with the long pointy ears is named (Jadzia) Dax. There's also a couple
in there of my floppy-eared bestie, named Hippo. They're both tiny little
10-pound~ish terrior mixes. I'm thinking maybe I'll collage a ton of little dog-nose doodles together into a massive post some time.

Drawn 2023-02-12 pen and ink (Noodler Ahab w/FPR Ultraflex EF), about A5 sized paper. `#lifedrawing` `#inkdrawing` `#fountainpen`

image::337745471_522036723450354_1416085482560438235_n_17951188331587637.jpg[]
image::337954260_6237484732976442_8985015476336813520_n_17959519922388094.jpg[]
image::338241182_573272297914401_7972055157067646507_n_17982824713869339.jpg[]
