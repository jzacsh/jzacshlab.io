+++
title = "Another portrait at Platform - made 20230214"
type = "post"
date = "2023-04-26"
publishdate = "2023-04-26"
+++

From another fun figure-drawing night at https://instagram.com/platformstudioschicago[@platformstudioschicago, window=_blank] a while back. Really not sure of who the model was as the likeness is so off, but I still enjoy this drawing.

Drawn 2023-02-14 in pen & ink (probably my Ahab+FPR-ultraflex setup), about A5-sized paper. `#lifedrawing` `#livemodel` `#fountainpen` `#inkdrawing`

image::343437004_555653973360367_1782874136597433477_n_18062113279382141.jpg[]
