+++
title = "Meerkat pages at Lincoln Park Zoo - made 2023-04-22"
type = "post"
date = "2023-09-09"
publishdate = "2023-09-09"
+++

Meerkat pages! at https://instagram.com/lincolnparkzoo[@lincolnparkzoo, window=_blank]

Some favorite moments from this Spring. These can be tricky to draw because they're so fidgety even when they're sitting relatively still way up high on their security-guard hill at this zoo.

The diagramming on the first page is something I started doing when drawing the vultures taught me that I didn't really understand the vultures by memory. I'd continuously be surprised that I'd draw them well from observation, but then look away for a matter of seconds and not be able to draw from memory. So out of frustration I started diagramming the vultures' features to prove to myself that I was paying attention. I've really liked this practice and now do this sometimes before I even test my memory. Unsure if it helps my memory but it certainly makes drawing from observation much faster and more fun.

Drawings from 2023-04-22, 2023-05-13 in pen & ink (with some ink-wash) `#fromlife` `#zoo` `#meerkat` `#inkdrawing` `#fountainpen` `#penandink` `#lineart`

image::376269716_285140707564187_4364040675496673136_n_17948978006677267.jpg[]
image::377229410_1775479712867156_8430954576138991239_n_18043936789467872.jpg[]
image::376013274_804583494698961_4992608794385907460_n_18080641291381408.jpg[]
image::377412525_1682574018902753_2586176613511416179_n_18059517169446326.jpg[]
image::376632441_3242228809403868_5357793947760140959_n_18013658473723700.jpg[]
image::376741752_338256358718484_395301985736415241_n_18006340048987399.jpg[]
