+++
title = "Platform Studios model on stool - made 20230309"
type = "post"
date = "2023-06-19"
publishdate = "2023-06-19"
+++

Three pages from early March, at my favorite spot: https://instagram.com/platformstudioschicago[@platformstudioschicago, window=_blank].

Some of my favorite drawings are with shorter poses - around 2 minutes. I think the one with arms up is probably(?) 5 minutes, but I didn't write it down that time.

Drawn 2023-03-09 and 2023-03-14 in pen & ink on A5ish paper (don't recall the fountain pen; given how thin the lines are, it's probably my beloved Pilot KakÃ¼no EF; the one with arms up is probably something with a flexible nib though) `#livemodel` `#lifedrawing` `#inkdrawing` `#fountainpen`

image::354480775_1665627180573441_1458510105823289799_n_18040675948472603.jpg[]
image::355078282_814452069827992_5564504273640953114_n_18070288579379208.jpg[]
image::354494272_910076643429359_8463272990011979359_n_17968834052290894.jpg[]
