+++
title = "Pen & Ink: Studying, Learning as Continued Engagement"
type = "page"
+++

## Overview: Learning & Art, Why?

I love keeping a sketchbook: [drawing with pen and ink](/drawings) and a big
element that keeps me engaged, aside from seeing others' works or getting
feedback on mine, is learning. This comes up enough in conversations with other
artists, like exchanging favorites of current art books or resources, that I
decided it would help me to write it down somewhere.

## Recent Learnings

Here's my own efforts that I can speak to (and maybe even recommend):

- 2024-11: "Upper Arm" with Dr. Bouftass' at Palette & Chisel
- 2024-10: "Leg & Foot" with Dr. Bouftass' at Palette & Chisel
- 2024-09: Glenn Vilppu's same workshop, but in Vienna, Austria
- 2024-08: "Head & Hands" at Dr. Said Bouftass' in-person (Palette & Chisel,
  2024-07-10 to 2024-08-28).
- 2024-07: self-study of head via
- 2024-06: Glenn Vilppu's week-long, 9-5 figure workshop
- 2023-12: nma.art studies
- 2023-present: daily figure drawing, self-study with books like
  [Bammes, Vilppu, Tom Fox](#resources).
- 2003-2007: various college courses (and things like the now defunct Governer
  School of the Arts 2005).

## Resources

### Books

Here's some other resourcesI can speak to--anything from "I couldn't get through
this" to "love this and read use it regularly":

* [Bammes' "Complete Guide to Anatomy for Artists & Illustrators"](https://www.goodreads.com/en/book/show/30316175-the-complete-guide-to-anatomy-for-artists-illustrators)
* Glenn Vilppu's ["Drawing Manual"](https://www.goodreads.com/book/show/3070146-vilppu-drawing-manual)
  * [latest edition](https://www.artsupplywarehouse.com/products/vilppu-drawing-manual--how-to-draw-figures-with-gesture--form-and-feeling%7C9780578824246.html) from [2022](https://www.instagram.com/newmastersacademy/p/Cl6Gb6VrotW)
* ["*Drawing Lessons from the Great Masters*" by Robert Beverly Hale](https://www.goodreads.com/book/show/257965.Drawing_Lessons_from_the_Great_Masters)
* "[Complete Guide to Drawing From Life](https://www.goodreads.com/book/show/35100546-bridgman-s-complete-guide-to-drawing-from-life)" by George B. Bridgman
  * 5th Ed. (ISBN-13 9781454926535\)
* "[Anatomy for Artists: Drawing Form & Pose](https://www.goodreads.com/book/show/61187735-anatomy-for-artist)" by Tom Fox
* ["The Electric State"](https://www.goodreads.com/book/show/36836025-the-electric-state) by Simon Stålenhag
* "[Mastering Atmosphere & Mood in Watercolor](https://www.goodreads.com/book/show/2130526.Mastering_Atmosphere_Mood_in_Watercolor)" by Joseph Zbukvic
* "[The Skillful Huntsman: Visual Development of a Grimm Tale](https://www.goodreads.com/book/show/334181.The_Skillful_Huntsman)" by Scott Robertson
* "[Figure Drawing: Design and Invention](https://www.goodreads.com/book/show/28454440-figure-drawing)" by Michael Hampton
  * 2nd Ed. (ISBN-13  978-0615272818) with red accent on cover
* "[Anatomical terms of bone](https://en.wikipedia.org/wiki/Anatomical_terms_of_bone)" by Wikipedia
  * [A5-booklet PDF here](https://drive.google.com/file/d/1GgKySjuGIvLcv3YFO4-5PXOSoJ2PL-ha/preview)
* "[Anatomical terms of location](https://en.wikipedia.org/wiki/Anatomical_terms_of_location)" by Wikipedia
  * [A5-booklet PDF here](https://drive.google.com/file/d/1ONofW1Uckzho2lT70lDPSVLUBoQHAFJP/preview)
* "[Anatomical terminology](https://en.wikipedia.org/wiki/Anatomical_terminology)" by Wikipedia

### Auto-Curriculumn

[Here's a sort of "auto didact's art curriculumn"](/figure-direction) I've
written myself with input from friends with similar interests. I imagine this
list is only really interesting if you have an obsession with figure and
portraiture drawing. It's useful to me, and hopefully to others, in that it's a
directed collection of resources towards a particular focus. I don't imagine
it's realistic (or maybe even interesting) for anyone to rush through this list
like one does through an actual academic institution's curriculum.
