TMPSRV    := tmp
SRCD      := src
ASSETS    := assets
BINDIR    := $(SRCD)/bin
JSDIR     := $(ASSETS)/js

TAGHASH   := TAG_VERSION_HASH
TAGTREE   := TAG_VERSION_TREE
TAGDATE   := TAG_VERSION_DATE
GIT_HEAD  := `git symbolic-ref --short HEAD`
GIT_VER   := $(shell git show-ref --hash heads/$(GIT_HEAD) | tee | cut -c 1-10)
GIT_TREE  := "https://gitlab.com/jzacsh/jzacsh.gitlab.io/-/tree/$(GIT_VER)"
GIT_DATE  := `git show -s --format='%cd' $(GIT_VER)`

STATIC_DIR_ROOT := static
ARTDEX_JSON     := $(STATIC_DIR_ROOT)/img/artwork/sketchbook.json

# TODO nitpick: reorganize recipes so hugo (`compile`) doesn't run twice in this
# step.
devserver: buildsome
	hugo server --bind 0.0.0.0

watch:
	run-dir-change -w 7 -r -c 'make lint sidecar_tag' $(ASSETS) $(SRCD)

# WARNING: nothing should run here that doesn't run with buildall! ie: this
# should be a strict subset of buildall
buildsome: lint compile sidecar_tag

# WARNING: _thumb is hardcoded into art.ts internal logic
lint_build_index: list_orphaned_thumbs
	deno check $(BINDIR)/art-index-build.ts

list_orphaned_thumbs:
	@$(BINDIR)/ensure-no-orphaned-thumbs.sh $(STATIC_DIR_ROOT) $(STATIC_DIR_ROOT)/img/_thumb

lint_runtime_ts:
	deno check $(JSDIR)/ts/gallery.ts

# TODO introduce auto-formatting
lint: lint_build_index lint_runtime_ts

# superset of build steps; this is split in half because some of the steps are
# painfully slow and only necessary when deploying but not when locally
# developing.
buildall: clean buildsome sidecar_artindex

# ensure index of CDN's artwork isn't stale; this MUST be before dirty-repo
# check
# TODO add flag for dirty-check to sidecar_artindex script to skip dimension
# calculations and compare two JSON outputs, to save time here.
sidecar_artindex: lint_build_index
	$(BINDIR)/art-index-build.ts "$(STATIC_DIR_ROOT)"
	$(MAKE) artindexdiff

artindexdiff:
	$(BINDIR)/art-index-compare.sh "$(ARTDEX_JSON)"

compile: setupbuild
	hugo

setupbuild:
	mkdir -p $(TMPSRV)/

sidecar_tag: TAG_SRC=$(shell find $(TMPSRV) -type f -name '*.html')
sidecar_tag:
	sed --in-place "s|$(TAGHASH)|$(GIT_VER)|g"  $(TAG_SRC)
	sed --in-place "s|$(TAGTREE)|$(GIT_TREE)|g" $(TAG_SRC)
	sed --in-place "s|$(TAGDATE)|$(GIT_DATE)|g" $(TAG_SRC)

clean:
	$(RM) -rf $(TMPSRV)/*

# TODO put s/lint buildsome/buildall/ back in here once sidecar_artindex no longer
# produces metadata in JSON where all values go into all categories.
deploy: clean lint buildsome
	$(BINDIR)/gitlab-pages.sh $(GIT_VER) $(TMPSRV)

deployGcs: buildall
	cd "$(TMPSRV)/" && gsutil rsync -d -r gs://$(GCS_BUCKET) .

deployAws: buildall
	cd "$(TMPSRV)/" && aws s3 sync --acl public-read . s3://$(S3_BUCKET)

deployKcdn: buildall
	rsync --recursive "$(TMPSRV)/" keycdn:zones/${KYCDN_ZONE}

.PHONY: devserver deploy clean sidecar_tag buildall buildsome sidecar_artindex setupbuild compile lint lint_runtime_ts lint_build_index watch
